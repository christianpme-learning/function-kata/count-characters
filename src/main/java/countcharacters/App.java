package countcharacters;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

public class App 
{
	public Hashtable<Character, Integer> countCharacters(String string) {
        Hashtable<Character, Integer> dictionary = new Hashtable<Character, Integer>(){

            private static final long serialVersionUID = 1L;

            private List<Character> keysSorted = new ArrayList<>();

            @Override
            public synchronized Integer put(Character key, Integer value) {
                if(get(key)==null)
                    keysSorted.add(key);

                return super.put(key, value);
            }

            @Override
            public synchronized String toString(){
                StringBuilder sb = new StringBuilder();

                int index=0;
                for(Character key : keysSorted){
                    sb.append(key.toString());
                    sb.append(':');
                    sb.append(get(key));
                    
                    ++index;
                    if(keysSorted.size()!=index)   
                        sb.append(", ");
                }
                return sb.toString();
            }
        };

        for(Character character : string.toCharArray()){
            if(character.equals(' ')){
                character = '_';
            }
            int count=0;
            if(dictionary.get(character)!=null){
                count = dictionary.get(character);
            }
            dictionary.put(character, count+1);
        }

		return dictionary;
    }
}
