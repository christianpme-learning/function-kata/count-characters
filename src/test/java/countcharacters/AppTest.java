package countcharacters;

import static org.junit.Assert.assertTrue;

import java.util.Dictionary;

import org.junit.Test;

public class AppTest 
{
    @Test
    public void countCharactersTest(){
        App app = new App();
        Dictionary<Character, Integer> actual = app.countCharacters("Das darf nicht sein");

        assertTrue(1==actual.get('D'));
        assertTrue(2==actual.get('a'));
        assertTrue(2==actual.get('s'));
        assertTrue(3==actual.get('_'));
        assertTrue(1==actual.get('d'));
        assertTrue(1==actual.get('r'));
        assertTrue(1==actual.get('f'));
        assertTrue(2==actual.get('n'));
        assertTrue(2==actual.get('i'));
        assertTrue(1==actual.get('c'));
        assertTrue(1==actual.get('h'));
        assertTrue(1==actual.get('e'));
        assertTrue(1==actual.get('t'));
    }

    @Test
    public void countCharactersToStringTest(){
        App app = new App();
        String actual = app.countCharacters("Das darf nicht sein").toString();

        //"e:1, t:1" kata solution
        String expected = "D:1, a:2, s:2, _:3, d:1, r:1, f:1, n:2, i:2, c:1, h:1, t:1, e:1";

        assertTrue(expected.equals(actual));
    }
}
